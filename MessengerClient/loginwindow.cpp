#include "loginwindow.h"

LoginWindow::LoginWindow(QWidget *parent) : QWidget(parent)
{
    login = new QLineEdit;
    password = new QLineEdit;
    button = new QPushButton;
    reg_b = new QPushButton("Регистрация");
    l_status = new QLabel("Получаем информацию о сервере");
    regW = new RegistrationWindow;
    host = new QLineEdit;
    port = new QSpinBox;
    storePassword = new QCheckBox;

    QLabel* header = new QLabel("<h1>Вход</h1>");
    QLabel* title = new QLabel("Developed by Andrey Bogdanov");
    QLabel* l_login = new QLabel("&Введите логин");
    QLabel* l_password = new QLabel("&Введите пароль");
    QLabel* l_host = new QLabel("&Адрес");
    QLabel* l_port = new QLabel("&Порт");
    QLabel* l_store_password = new QLabel("Запомнить пароль");

    l_login->setBuddy(login);
    l_password->setBuddy(password);
    l_host->setBuddy(host);
    l_port->setBuddy(port);
    port->setRange(0, 99999);

    password->setEchoMode(QLineEdit::Password);
    button->setEnabled(false);
    button->setText("Авторизация");

    //Signals\slots setup
    connect(button, SIGNAL(clicked(bool)), this, SLOT(slotButtonClicked()));
    connect(reg_b, SIGNAL(clicked(bool)), this, SLOT(slotRegistration()));
    connect(host, SIGNAL(textChanged(QString)), this, SLOT(hostDataChanged()));
    connect(port, SIGNAL(valueChanged(int)), this, SLOT(hostDataChanged()));

    //Layout setup
    //Address select layout setup
    QHBoxLayout* host_address_labels = new QHBoxLayout;
    host_address_labels->addWidget(l_host);
    host_address_labels->addStretch(16);
    host_address_labels->addWidget(l_port);

    QHBoxLayout* password_storage = new QHBoxLayout;
    password_storage->addWidget(storePassword);
    password_storage->addWidget(l_store_password);

    QHBoxLayout* host_address = new QHBoxLayout;
    host_address->addWidget(host);
    host_address->addWidget(port);

    QVBoxLayout* vert = new QVBoxLayout;
    vert->addWidget(header, Qt::AlignCenter);
    vert->addLayout(host_address_labels);
    vert->addLayout(host_address);
    vert->addWidget(l_login);
    vert->addWidget(login);
    vert->addWidget(l_password);
    vert->addWidget(password);
    vert->addWidget(button);
    vert->addWidget(reg_b);
    vert->addLayout(password_storage);
    vert->addWidget(l_status);
    vert->setAlignment(header, Qt::AlignCenter);
    vert->addWidget(title);

    QHBoxLayout* main = new QHBoxLayout;
    main->addSpacing(1);
    main->addLayout(vert);
    main->addSpacing(1);

    setLayout(main);
    setWindowIcon(QIcon("/home/andrey/ProgrammingContest_18.03/client/icon.png"));

    //Load settings
    QSettings settings;
    host->setText(settings.value("Settings/LastHost", "localhost").toString());
    port->setValue(settings.value("Settings/LastPort", 2323).toInt());
    login->setText(settings.value("Settings/LastLogin", "").toString());
    password->setText(settings.value("Settings/LastPassword", "").toString());
    if(password->text() != "")
        storePassword->setChecked(true);
}

void LoginWindow::hostDataChanged(){
    emit hostChanged(host->text(), port->value());
}

void LoginWindow::closeEvent(QCloseEvent *event){
    exit(0);
}

void LoginWindow::sendHostData(){
    emit hostChanged(host->text(), port->value());
}

void LoginWindow::slotButtonClicked(){
    QByteArray arr = password->text().toUtf8();
    emit loginSignal(login->text(), QString(QCryptographicHash::hash(arr, QCryptographicHash::Sha256).toHex()));
    l_status->setText("Запрос отправлен!");
}

void LoginWindow::setServerSatatus(QString status){
    if(status == "accessable"){
        l_status->setText("Подключение разрешено!");
        button->setEnabled(true);
        reg_b->setEnabled(true);
        QSettings settings;
        settings.setValue("Settings/LastHost", host->text());
        settings.setValue("Settings/LastLogin", login->text());
        settings.setValue("Settings/LastPort", port->value());
        if(storePassword->isChecked()){
            settings.setValue("Settings/LastPassword", password->text());
        } else {
            settings.setValue("Settings/LastPassword", "")  ;
        }
    } else {
        l_status->setText("Сервер недоступен!");
        serverAccessable = false;
        button->setEnabled(false);
        reg_b->setEnabled(false);
    }
}

void LoginWindow::setAuthorizationStatus(bool status){
    if(!status){
        QMessageBox::critical(0, "QtMessenger", "Неверный логин или пароль!", QMessageBox::Yes);
        l_status->setText("Проверьте введенные данные");
        password->clear();
    } else {
        this->hide();
    }
}

void LoginWindow::slotRegistration(){
    regW->show();
    login->clear();
    this->hide();
}

RegistrationWindow::RegistrationWindow(QWidget *parent) :  QWidget(parent){
    login = new QLineEdit;
    password = new QLineEdit;
    button = new QPushButton;

    QLabel* header = new QLabel("<h1>Регистрация</h1>");
    QLabel* l_login = new QLabel("&Придумайте себе имя");
    QLabel* l_password = new QLabel("&Придумайте надежный пароль");

    l_login->setBuddy(login);
    l_password->setBuddy(password);

    password->setEchoMode(QLineEdit::Password);
    button->setText("Регистрация");

    connect(button, SIGNAL(clicked(bool)), this, SLOT(slotButtonClicked()));

    //Layout setup
    QVBoxLayout* vert = new QVBoxLayout;
    vert->addWidget(header);
    vert->addWidget(l_login);
    vert->addWidget(login);
    vert->addWidget(l_password);
    vert->addWidget(password);
    vert->addWidget(button);
    vert->setAlignment(header, Qt::AlignCenter);

    QHBoxLayout* main = new QHBoxLayout;
    main->addSpacing(1);
    main->addLayout(vert);
    main->addSpacing(1);

    this->setLayout(main);
}

void RegistrationWindow::setRegistrationStatus(bool data){
    if(!data && !(this->isHidden())){
        QMessageBox::critical(0, "QtMessenger", "Регистрация провалена! Используйте другое имя");
        login->clear();
        password->clear();
        button->setEnabled(true);
    } else{
        this->hide();
    }
}

void RegistrationWindow::slotButtonClicked(){
    if(login->text().size() < 4){
        QMessageBox::information(0, "QtMessenger", "Логин должен быть больше 3 символов", QMessageBox::Yes);
        login->clear();
        return;
    }
    if(password->text().size() <= 5){
         QMessageBox::information(0, "QtMessenger", "Пароль должен быть больше 5 символов", QMessageBox::Yes);
         password->clear();
         return;
    }
    QByteArray arr = password->text().toUtf8();
    emit registerSignal(login->text(), QString(QCryptographicHash::hash(arr, QCryptographicHash::Sha256).toHex()));
    button->setEnabled(false);
}
