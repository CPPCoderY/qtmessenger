#pragma once

#include <QObject>
#include <QTcpSocket>
#include <QString>
#include <QDataStream>
#include <QStack>
#include <QThread>
#include <QTimer>
#include "clientcore.h"

struct ServerClient;

class MessengerServer : public QObject
{
    Q_OBJECT
private:
    QTcpSocket* server;
    QString host;
    QTimer* timer;
    int port;
    quint64 id;
    quint16 nextBlockSize;

    //Input methods (Reading and handling data from the server)
    void handleMessage(QDataStream* in);
    void readMessage(QDataStream* in);
    void readLoginData(QDataStream* in);
    void readUsersInfo(QDataStream* in);

    void readRegistrationData(QDataStream* in);
    void readSyncFile(QDataStream* in);
    void readFriendRequest(QDataStream* in);
    void readFriendRequestAnswer(QDataStream* in);
    void readPublicChats(QDataStream* in);

    //Output methods (sending data from client to the server)
    void sendMessageByID(QString message, quint64 id);
public:
    MessengerServer();

private slots:
    void slotReadMessage();
    void slotError(QAbstractSocket::SocketError err);
    void connect();

public slots:
    void sendMessage(QString m, quint64 id);
    void slotSendLoginData(QString name, QString password);
    void sendFriendsRequest(quint64 id, QString message);
    void sendFriendsRequestAnswer(quint64 pendingFriend, bool status, QString chatName);
    void sendSyncRequest(QMap<quint64, QString> hashes);
    void sendRegisrationData(QString login, QString password);
    void slotSendPublicChatJoinRequest(quint64 id);
    void connectToHost(const QString& host, int port);
    void slotCreatePublicChat(QString name);

signals:
    void serverStatusGot(QString status);
    void messageGot(QString m, quint64 senderID, QString senderLogin);
    void authorizationAnswerGot(bool status, quint64 id = 0, QString permissions = "user");
    void usersDataGot(QString name, QMap<QString, ServerClient> users);

    void registrationDataGot(bool status);
    void syncFileGot(quint64 dialogID, QByteArray file);
    void friendRequestGot(QString author, quint64 authorID, QString message);
    void friendRequestAnswerGot(QString name, quint64 id, QString dialogName, QString anotherUser);
};

