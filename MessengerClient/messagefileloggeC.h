#ifndef MFL
#define MFL

#include <QFile>
#include <QString>
#include <QtXml>
#include <QCryptographicHash>
#include <chat.h>

class Chat;

class MessageFileLogger{
private:
    QString SAVED_CHATS_DIR = QDir::separator() +QString( "savedChats") + QDir::separator();
    Chat* readChatFromFile(quint64 chatID);
public:
    MessageFileLogger(QString userName);
    void writeMessageToChat(quint64 chatID, QString author, QString message, QString userFolder);
    void processSync(quint64 id, QByteArray new_file);
    void createChatFile(quint64 id, QString chat_name, QString userFolder, bool isPublic = false);
    QMap<quint64, QString> getHashes();
    QMap<quint64, Chat*> getAllChats();
};

#endif
//MFL
