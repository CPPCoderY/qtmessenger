QT += core widgets
QT += network gui xml
CONFIG += c++11

TARGET = MessengerClient
CONFIG -= app_bundle
CONFIG += release

TEMPLATE = app

SOURCES += main.cpp \
    messengerserver.cpp \
    chatmainwindow.cpp \
    clientcore.cpp \
    chatarea.cpp \
    clientspanel.cpp \
    chat.cpp \
    loginwindow.cpp \
    messagefileloggerC.cpp \
    chatspanel.cpp

HEADERS += \
    messengerserver.h \
    chatmainwindow.h \
    clientcore.h \
    chatarea.h \
    clientspanel.h \
    chat.h \
    loginwindow.h \
    messagefileloggeC.h \
    chatspanel.h
    servercore.h
