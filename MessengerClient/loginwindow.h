#pragma once

#include <QObject>
#include <QtWidgets>
#include <QCryptographicHash>

class RegistrationWindow;
class ClientCore;

class LoginWindow : public QWidget
{
    //Some costyls
    friend class ClientCore;

    Q_OBJECT

private:
    QLineEdit* login;
    QLineEdit* password;
    QLineEdit* host;
    QSpinBox* port;
    QPushButton* button;
    QPushButton* reg_b;
    QCheckBox* storePassword;
    QLabel* l_status;

    bool serverAccessable = true;

    RegistrationWindow* regW;
protected:
    virtual void closeEvent(QCloseEvent *event);
public:
    LoginWindow(QWidget *parent = 0);
    void setServerSatatus(QString status);
    void setAuthorizationStatus(bool status);
    void sendHostData();
signals:
    void loginSignal(QString name, QString password);
    void hostChanged(QString host, int port);
private slots:
    void slotButtonClicked();
    void slotRegistration();
    void hostDataChanged();
};

//===================================================================
//===================================================================
//===================================================================

class RegistrationWindow : public QWidget
{
    friend class ClientCore;
    Q_OBJECT
private:
    QLineEdit* login;
    QLineEdit* password;
    QPushButton* button;
public:
    RegistrationWindow(QWidget *parent = 0);
    void setRegistrationStatus(bool data);
signals:
    void registerSignal(QString name, QString password);
private slots:
    void slotButtonClicked();
};

