#include "chatarea.h"

ChatArea::ChatArea(Chat* newChat, quint64 newAddress) : chat(newChat), constantAddress(newAddress)
{
    display = new QTextEdit;
    input = new QLineEdit;
    sendButton = new QPushButton;
    sendButton->setEnabled(false);
    status = new QLabel("Ни один чат не открыт.");

    //Layaout setup
    QHBoxLayout* writingArea = new QHBoxLayout;
    sendButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    sendButton->setText("Отправить");
    writingArea->addWidget(input);
    writingArea->addWidget(sendButton);
    QVBoxLayout* mainArea = new QVBoxLayout;
    mainArea->addWidget(status);
    mainArea->addWidget(display);
    mainArea->addLayout(writingArea);
    this->setLayout(mainArea);

    display->setReadOnly(true);
    if(newChat == 0){
        display->append("[Swallow messenger] \n Нет начатых чатов! Выберите пользователя из списка справа!");
    } else {
        loadChat();
    }
    input->setToolTip("Введите сообщение");
    sendButton->setToolTip("Нажмите для отправки!");

    //          Signals/slots setup
    QObject::connect(sendButton, SIGNAL(clicked(bool)), this, SLOT(buttonPressed()));

}

void ChatArea::loadChat(){
    sendButton->setEnabled(true);
    display->clear();
    int size = chat->getSize();
    for(int i = 0; i < size; i++){
        QString senderLogin;
        QString message;
        chat->getNextMessage(i, senderLogin, message);
        printMessage(message, senderLogin);
    }
}

void ChatArea::buttonPressed(){
    QString message;
    message = input->text();
    if(message.contains("<") || message.contains(">")){
        QMessageBox::critical(this, "QtMessenger", "Сообщение не может содержать знаки '< >'", QMessageBox::Yes);
        return;
    }
    input->clear();
    emit writedNewMessage(message, constantAddress);
    printMessage(message, "ВЫ");
    chat->append("ВЫ", message);
}

void ChatArea::printMessage(QString message, QString senderLogin){
    QString record;
    record = record + "[" + senderLogin + "]\n" + message;
    display->append(record);
}

void ChatArea::bindChat(Chat *newChat){
    chat = newChat;
    display->clear();
    constantAddress = chat->id;
    qDebug() << "Binded chat, id is" << constantAddress;
    loadChat();
    QString s_status = "Чат " + chat->name;
    status->setText(s_status);
}

