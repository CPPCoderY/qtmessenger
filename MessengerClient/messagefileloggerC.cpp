#include "messagefileloggeC.h"

MessageFileLogger::MessageFileLogger(QString userName) {
    QString dirName = QDir::currentPath() + SAVED_CHATS_DIR;
    QDir dir(dirName);
    if(!dir.exists()){
        qDebug() << "Chats directory does not exists";
        dir.mkdir(dir.path());
    }
    dirName.clear();
    SAVED_CHATS_DIR += userName += QDir::separator();
    qDebug() << SAVED_CHATS_DIR;
    dirName = QDir::currentPath() + SAVED_CHATS_DIR;
    QDir dir2(dirName);
    if(!dir2.exists()){
        qDebug() << "Chats directory of user does not exists";
        dir2.mkdir(dir2.path());
    }
}

Chat* MessageFileLogger::readChatFromFile(quint64 chatID){
    QString fileName = QDir::currentPath() + SAVED_CHATS_DIR + QString::number(chatID) + ".chh";
    QFile file(fileName);
    file.open(QFile::ReadOnly);
    if(file.size() == 0)
        return 0;
    QString name, d_name, author, message;
    bool isPublic;
    QXmlStreamReader reader(&file);
    reader.readNextStartElement();
    name = reader.name().toString();
    if(name == "dialogs"){
        d_name = reader.attributes().first().value().toString();
        QVariant var = reader.attributes().last().value().toInt();
        isPublic = var.toBool();
        qDebug() << "Reading dialog" << d_name;
    } else {
        qWarning("XML format error! \".chh\" file syntax damaged!");
        return 0;
    }
    Chat* c = new Chat(chatID, d_name, isPublic);
    while(!reader.atEnd()){
        reader.readNext();
        name = reader.name().toString();
        if(name == "message"){
            author = reader.attributes().first().value().toString();
            message = reader.readElementText();
            qDebug() << "Message, author is " << author << "\n message is" << message;
            c->append(author, message);
        }
    }
    return c;
}

void MessageFileLogger::createChatFile(quint64 id, QString chat_name, QString userFolder, bool isPublic){
    QString fileName = QDir::current().absolutePath() + "/savedChats/" + userFolder  + QDir::separator() + QString::number(id) + ".chh";
    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    QTextStream writer(&file);
    QXmlStreamWriter xml(&file);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeStartElement("dialogs");
    xml.writeAttribute("name", chat_name);
    xml.writeAttribute("isPublic", QString::number(qint8(isPublic)));
    writer << ">";
    file.close();
}

void MessageFileLogger::writeMessageToChat(quint64 chatID, QString author, QString message, QString userFolder){
    qDebug() << "Writing message to " << chatID;
    QString fileName = QDir::current().absolutePath() + "/savedChats/" + userFolder + QDir::separator() + QString::number(chatID) + ".chh";
    QFile file(fileName);
    file.open(QIODevice::Append);
    QXmlStreamWriter xml(&file);
    xml.setAutoFormatting(true);
    xml.writeStartElement("message");
    xml.writeAttribute("author", author);
    xml.writeCharacters(message);
    xml.writeEndElement();
}

QMap<quint64, QString> MessageFileLogger::getHashes(){
    QMap<quint64, QString> hashes;
    QDir dir(QDir::currentPath() + SAVED_CHATS_DIR);
    QStringList names = dir.entryList(QDir::Files);
    foreach (QString file_name, names) {
        QFile file(dir.absoluteFilePath(file_name));
        file.open(QIODevice::ReadOnly);
        QByteArray bytes = file.readAll();
        QString hash = QCryptographicHash::hash(bytes, QCryptographicHash::Sha256).toHex();
        hashes[file_name.remove(".chh").toLongLong()] = hash;
        file.close();
    }
    return hashes;
}

void MessageFileLogger::processSync(quint64 id, QByteArray new_file){
    QString file_name = QDir::currentPath() + SAVED_CHATS_DIR + QString::number(id) + ".chh";
    QFile file(file_name);
    file.open(QIODevice::WriteOnly);
    file.write(new_file);
    file.close();
}

QMap<quint64, Chat*> MessageFileLogger::getAllChats(){
    QMap<quint64, Chat*> chats;
    QDir dir(QDir::currentPath() + SAVED_CHATS_DIR);
    QStringList names = dir.entryList(QDir::Files);
    if(names.isEmpty()){
        return chats;
    }
    foreach(QString file_name, names){
        quint64 id = file_name.remove(".chh").toLongLong();
        chats[id] =  readChatFromFile(id);
    }
    return chats;
}
