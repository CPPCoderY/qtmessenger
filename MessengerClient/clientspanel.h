#pragma once

#include <QtWidgets>
#include <QString>
#include <QStack>
#include <QMap>

#include "clientcore.h"

struct ServerClient;

class ClientsPanel : public QWidget
{
    Q_OBJECT
private:
    QTreeWidget* twg;
    QMap<QString, QTreeWidgetItem*> headers;
public:
    ClientsPanel();
    QPushButton* newPublic;
    void drawList(QString group,  QMap<QString, ServerClient>  newUsers);
    void addChat(QString group, ServerClient newClient);
signals:
    void chatOpenRequest(QString name);
    void chatJoinRequest(QString chatName);
    void createNewPublic(QString name);
public slots:
    void slotUpdateFriends(QString group, QMap<QString, ServerClient> newUsers);
private slots:
    void slotButtonClicked();
    void slotItemClicked(QTreeWidgetItem* twi, int collumn);
};

