#pragma once

#include <QtWidgets>
#include <QObject>
#include <QMessageBox>

#include "messengerserver.h"

class Chat;

class ChatArea : public QWidget
{
    Q_OBJECT
private:
    QTextEdit* display;
    QLineEdit* input;
    QPushButton* sendButton;
    QLabel*  status;
    Chat* chat;
    quint64 constantAddress;
    void loadChat();
public:
    ChatArea(Chat* newChat = 0, quint64 newAddress = 0);
    void printMessage(QString message, QString senderLogin);
    void bindChat(Chat* newChat);
signals:
    void writedNewMessage(QString m, quint64 address);
private slots:
    void buttonPressed();
};
