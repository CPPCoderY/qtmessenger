#include "chat.h"

Chat::Chat(quint64 id, QString chatName, bool isPublic) : id(id), name(chatName), isPublic(isPublic) { }

Chat::Chat(quint64 id, QString chatName, QStringList authors) : id(id), name(chatName), authors(authors) {}

void Chat::append(QString name, QString mes){
    Message *message = new Message;
    message->author = name;
    message->text = mes;
    history.append(message);
    if(!authors.contains(name)){
        authors.append(name);
        if(authors.size() > 2){
            isPublic = true;
        }
    }
}

void Chat::getNextMessage(int id, QString &sender, QString &message){
    Message* mes = history[id];
    sender = mes->author;
    message = mes->text;
}

int Chat::getSize(){
    return history.size();
}

bool Chat::containsAuthor(QString name){
    return authors.contains(name);
}
