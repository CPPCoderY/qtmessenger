#pragma once

#include <QVector>
#include <QString>

#include "clientcore.h"

struct Message {
    QString text;
    QString author;
};

class Chat
{
private:
    QVector<Message*> history;
    QStringList authors;
public:
    quint64 id;
    QString name;

    bool isPublic = false;

    Chat(quint64 id, QString chatName, bool isPublic);
    Chat(quint64 id, QString chatName, QStringList authors);

    int getAuthorsSize() {return authors.size();}
    void getNextMessage(int id, QString& sender, QString& message);
    void append(QString name, QString message);
    bool containsAuthor(QString name);
    QStringList getAuthors() {return authors;}
    int getSize();
};
