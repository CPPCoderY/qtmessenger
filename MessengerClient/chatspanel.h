#pragma once

#include <QtWidgets>

class ChatsPanel : public QWidget
{
    Q_OBJECT
private:
    QMap<QString, quint64> openedChats;
    QTreeWidget* twg;
    QMap<QString, QTreeWidgetItem*> headers;
public:
    ChatsPanel(QWidget *parent = 0);
    void addChat(QString type, QString chatName, quint64 chatID);
signals:
    void chatSelected(quint64 chatID);
public slots:
    void slotItemClicked(QTreeWidgetItem* twi, int collumn);
};
