#include <QThread>
#include <QObject>
#include <QtWidgets>
#include <QDebug>
#include <QDateTime>

#include "clientcore.h"
#include "messengerserver.h"
#include "chatmainwindow.h"

QString logFileName;

void metaTypeRegistration(){
    qRegisterMetaType<QMap<QString, ServerClient>>("QMap<QString, ServerClient>");
    qRegisterMetaType<QMap<quint64, QString>>("QMap<quint64, QString>");
}

void logMessage(QtMsgType type, const QMessageLogContext &context, const QString& message){
    QFile log(logFileName);
    if(!log.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
        return;
    }
#ifdef DEBUG
    std::cout << message.toStdString() << std::endl;
#endif
    QTextStream out(&log);
    switch(type){
    case QtDebugMsg:
        out << "[" << QTime::currentTime().toString() << "]" << "Debug: " << "    " << message << context.file << endl;
        break;
    case QtFatalMsg:
        out << "[" << QTime::currentTime().toString() << "]" << "Fatal Error: " << message << "    " << context.file << endl;
        break;
    case QtWarningMsg:
        out << "[" << QTime::currentTime().toString() << "]" << "Warning: " << message << "    " << context.file << endl;
        break;
    }
}


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    metaTypeRegistration();
    QDateTime time;
    logFileName = QString("[" + time.currentDateTime().toString(Qt::ISODateWithMs) + "] protocol.log");
    qInstallMessageHandler(logMessage);
    MessengerServer* server = new MessengerServer;
    ClientCore core(server);
    QThread t;
    server->moveToThread(&t);
    t.start();
    app.exec();
    return 0;
}
