#include "clientspanel.h"

ClientsPanel::ClientsPanel()
{
    //Layout setup
    QStringList list;
    newPublic = new QPushButton("Новый публичный чат");
    newPublic->setEnabled(false);
    list << "Name" << "Status";

    twg = new QTreeWidget;

    twg->setHeaderLabels(list);
    twg->setSortingEnabled(true);

    QTreeWidgetItem* headerItem = new QTreeWidgetItem(twg);
    headerItem->setText(0, "Пользователи");
    headers["#users#"] = headerItem;
    QTreeWidgetItem* headerItem2= new QTreeWidgetItem(twg);
    headerItem2->setText(0, "Общие чаты");
    headers["#publicChats#"] = headerItem2;
    QVBoxLayout* main_lay = new QVBoxLayout;
    main_lay->addWidget(twg);
    main_lay->addWidget(newPublic);
    this->setLayout(main_lay);

    //Signals\slots setup
    connect(twg, &QTreeWidget::itemClicked,
            this, &ClientsPanel::slotItemClicked);
    connect(newPublic, &QAbstractButton::clicked,
            this, &ClientsPanel::slotButtonClicked);
}

void ClientsPanel::drawList(QString group,  QMap<QString, ServerClient>  newUsers){
    qDebug() << "Clients panel drawing list, size is " << newUsers.size() << ". Group is " << group;
    QTreeWidgetItem* newItem = 0;

    //Remove old data1
    while(headers[group]->childCount() != 0)
        headers[group]->removeChild(headers[group]->child(0));

    //Drawing new data
    for(QMap<QString, ServerClient>::iterator it = newUsers.begin(); it != newUsers.end(); it++){
        newItem = new QTreeWidgetItem(headers[group]);
        newItem->setText(0, it.key());
        newItem->setText(1, it.value().status);
    }
}

void ClientsPanel::slotUpdateFriends(QString group, QMap<QString, ServerClient> newUsers){
    drawList(group, newUsers);
}

void ClientsPanel::slotButtonClicked()
{
    QString newChatName = QInputDialog::getText(this, "Новый публичный диалог", "Введите название публичного диалога, который вы хотите создать", QLineEdit::Normal);
    emit createNewPublic(newChatName);
}

void ClientsPanel::slotItemClicked(QTreeWidgetItem *twi, int collumn){
    if(twi != headers["#users#"] && twi != headers["#publicChats#"]){
        if(twi->parent() == headers["#users#"])
            emit chatOpenRequest(twi->text(0));
        else
            emit chatJoinRequest(twi->text(0));
    }
}

void ClientsPanel::addChat(QString group, ServerClient newClient){
    QTreeWidgetItem* newItem = new QTreeWidgetItem(headers[group]);
    newItem->setText(0, newClient.name);
    newItem->setText(1, QString::number(newClient.id));
    newItem->setText(2, newClient.status);
}
