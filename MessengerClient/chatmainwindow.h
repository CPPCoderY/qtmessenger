#pragma once

#include <QDebug>
#include <QStack>
#include <QString>
#include <QtWidgets>

#include "clientspanel.h"
#include "clientcore.h"
#include "chatarea.h"
#include "messengerserver.h"
#include "chatspanel.h"

class Chat;
class ClientsPanel;
class ChatArea;
class ChatsPanel;
struct ServerClient;

class ChatMW : public QWidget
{
    friend class ClientCore;
    Q_OBJECT
private:
    ClientsPanel* clientsPanel;
    ChatsPanel* chatsPanel;
    ChatArea* chatArea;
protected:
    virtual void closeEvent(QCloseEvent *event);
public:
    ChatMW();
    void openChat(Chat* c);
    void addChat(QString type, QString name, quint64 id);
    void reDrawList(QString group, QMap<QString, ServerClient>  newOnline);
    void acceptMessage(QString m, quint64 senderID, QString senderLogin);
signals:
    void messageWrited(QString m, quint64 id);
    void chatSelected(QString);
    void chatSelectedByID(quint64);
private slots:
    void slotChatOpenRequest(QString name);
    void slotChatSelected(quint64 id);
    void slotMessageWrited(QString m, quint64 id);
};
