#pragma once

#include <QObject>
#include <QMap>
#include <QStack>
#include <QString>

#include "messengerserver.h"
#include "chatmainwindow.h"
#include "chat.h"
#include "loginwindow.h"
#include "messagefileloggeC.h"

class MessengerServer;
class ChatMW;
class LoginWindow;
class MessageFileLogger;

struct ServerClient{
    QString name;
    quint64 id;
    QString status;
};

class ClientCore : QObject
{
    Q_OBJECT
private:
    QString youOwnLogin = "no_name";

    QMap<quint64, Chat*> openedChats;
    QMap<QString, ServerClient> usersOnline;
    QMap<QString, ServerClient> publicChats;

    MessageFileLogger* mfl;
    ChatMW* chatMW;
    LoginWindow* loginW;
    QString permissions;

    void updateChatsList();

public:
    ClientCore(MessengerServer* server);
signals:
    void chatOpenRequest(quint64 userID, QString message);
    void sendChatRequestStatus(quint64, bool, QString);
    void sendPublicChatJoinRequest(quint64);
    void sendSyncRequest(QMap<quint64, QString> hashes);
public slots:
    void slotMessageWrited(QString message, quint64 dialogID);
    void getMessage(QString message, quint64 senderID, QString senderLogin);
    void syncFileGot(quint64 id, QByteArray file);
    void getNewOnlineData(QString group,  QMap<QString, ServerClient>  newOnline);
    void slotUserClicked(QString name);
    void slotPublicChatClicked(QString name);
    void getAuthData(bool status, quint64 id, QString permissions);
    void getRegData(bool data);
    void getChatRequestStatus(QString name, quint64 chatID,  QString chatName, QString anotherName);
    void getChatRequest(QString name, quint64 id, QString message);
    void getServerStatus(QString status);
    void slotOpenedChatSelected(quint64 id);
    void slotServerError(QString error);
};
