#include "chatspanel.h"

ChatsPanel::ChatsPanel(QWidget *parent) : QWidget(parent)
{
    twg = new QTreeWidget;

    twg->setHeaderLabel("Открытые чаты");

    QTreeWidgetItem* privateChats = new QTreeWidgetItem(twg);
    privateChats->setText(0, "Личные чаты");
    headers["#private#"] = privateChats;
    QTreeWidgetItem* groupChats = new QTreeWidgetItem(twg);
    groupChats->setText(0, "Групповые чаты");
    headers["#public#"] = groupChats;

    //Layout setup
    QVBoxLayout* main_lay = new QVBoxLayout;
    main_lay->addWidget(twg);
    this->setLayout(main_lay);

    //Signals\slots setup
    QObject::connect(twg, SIGNAL(itemClicked(QTreeWidgetItem*,int)),
                     this, SLOT(slotItemClicked(QTreeWidgetItem*,int)));
    QObject::connect(twg, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)),
                     this, SLOT(slotItemClicked(QTreeWidgetItem*,int)));
}

void ChatsPanel::addChat(QString type, QString chatName, quint64 chatID){
    if((openedChats.contains(chatName) && openedChats[chatName] == chatID) || chatName == "")
        return;
    QTreeWidgetItem *newChat = new QTreeWidgetItem(headers[type]);
    newChat->setText(0, chatName);
    openedChats[chatName] = chatID;
}

void ChatsPanel::slotItemClicked(QTreeWidgetItem *twi, int collumn){
    if(twi != headers["#private#"] && twi != headers["#public#"]){
        emit chatSelected(openedChats[twi->text(0)]);
    }
}
