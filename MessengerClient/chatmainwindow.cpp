#include "chatmainwindow.h"

ChatMW::ChatMW()
{
    //Layout setup
    this->resize(680, 520);
    QHBoxLayout* main_lay = new QHBoxLayout;
    QVBoxLayout* leftSide = new QVBoxLayout;
    clientsPanel = new ClientsPanel;
    chatsPanel = new ChatsPanel;
    chatArea = new ChatArea;
    leftSide->addWidget(clientsPanel);
    leftSide->addWidget(chatsPanel);
    main_lay->addLayout(leftSide);
    main_lay->addWidget(chatArea);
    this->setLayout(main_lay);

    //Signals\slots setup
    connect(clientsPanel, SIGNAL(chatOpenRequest(QString)),
            this, SLOT(slotChatOpenRequest(QString)));
    connect(chatArea, SIGNAL(writedNewMessage(QString,quint64)),
            this, SLOT(slotMessageWrited(QString,quint64)));
   connect(chatsPanel, SIGNAL(chatSelected(quint64)),
           this, SLOT(slotChatSelected(quint64)));

   setWindowIcon(QIcon("/home/andrey/ProgrammingContest_18.03/client/icon.png"));
}

void ChatMW::acceptMessage(QString m, quint64 senderID, QString senderLogin){
    chatArea->printMessage(m, senderLogin);
}

void ChatMW::closeEvent(QCloseEvent *event){
    exit(0);
}

void ChatMW::slotMessageWrited(QString m, quint64 id){
    emit messageWrited(m, id);
}

void ChatMW::slotChatOpenRequest(QString name){
    emit chatSelected(name);
}

void ChatMW::openChat(Chat *c){
    chatArea->bindChat(c);
}

void ChatMW::reDrawList(QString group, QMap<QString, ServerClient>  newOnline){
    clientsPanel->drawList(group, newOnline);
}

void ChatMW::slotChatSelected(quint64 id){
    emit chatSelectedByID(id);
}

void ChatMW::addChat(QString type, QString name, quint64 id){
    chatsPanel->addChat(type, name, id);
}
