#include "messengerserver.h"

MessengerServer::MessengerServer() :
    nextBlockSize(0)
{
    server = new QTcpSocket(this);
    timer = new QTimer(this);

    //Signals \ slots setup
    QObject::connect(timer, SIGNAL(timeout()),
                     this, SLOT(connect()));
    QObject::connect(server, SIGNAL(readyRead()),
                     this, SLOT(slotReadMessage()));
    QObject::connect(server, SIGNAL(error(QAbstractSocket::SocketError)),
                     this,  SLOT(slotError(QAbstractSocket::SocketError)));

    timer->start(1000);
}

void MessengerServer::connectToHost(const QString &host, int port){
    this->host = host;
    this->port = port;
    server->disconnectFromHost();
}

void MessengerServer::slotCreatePublicChat(QString name)
{
    QByteArray bytes;
    QDataStream out(&bytes, QIODevice::WriteOnly);
    out << quint16(0) << quint8(11) << name;
    out.device()->seek(0);
    out  << quint16(bytes.size() - sizeof(quint16));
    server->write(bytes);
    server->flush();
}

void MessengerServer::connect(){
    if(server->state() == QAbstractSocket::UnconnectedState
            || server->state() == QAbstractSocket::HostNotFoundError
            || server->state() == QAbstractSocket::HostLookupState){
        server->connectToHost(host, port);
        server->waitForConnected(2 * 1000);
        emit serverStatusGot("Unaccessable");
    } else {
        emit serverStatusGot("accessable");
    }
}

void MessengerServer::slotReadMessage()
{
    QDataStream* in = new QDataStream(server);
    in->setVersion(QDataStream::Qt_5_3);
    handleMessage(in);
    delete in;
}

void MessengerServer::handleMessage(QDataStream *in){
    quint8 message_type;
    *in >> nextBlockSize;
    forever{
        if(!nextBlockSize){
            if(server->bytesAvailable() < sizeof(quint16)){
                return;
            }
            *in >> nextBlockSize;
        }
        if(server->bytesAvailable() < nextBlockSize) {
            return;
        } else {
            *in >> message_type;
            qDebug() << "Message key" << message_type;
            switch(message_type){
            case 1:
                readLoginData(in);
                break;
            case 2:
                readMessage(in);
                break;
            case 3:
                readUsersInfo(in);
                break;
            case 4:
                readSyncFile(in);
                break;
            case 5:
                readFriendRequest(in);
                break;
            case 6:
                readFriendRequestAnswer(in);
                break;
            case 8:
                readPublicChats(in);
                break;
            case 10:
                readRegistrationData(in);
                break;
            default:
                qWarning() << "Invalid message key";
                break;
            }
            nextBlockSize = 0;
        }
    }
}

void MessengerServer::readLoginData(QDataStream *in){
    bool loginStatus;
    QString permissions;
    *in >> loginStatus;
    qDebug() << loginStatus;
    if(loginStatus){
        *in >> permissions;
        qDebug() << "Authorization accepted";
        timer->stop();
        emit authorizationAnswerGot(loginStatus, id, permissions);
    } else {
        qDebug() << "Failed authorization!";
        emit authorizationAnswerGot(loginStatus);
    }
}

void MessengerServer::readMessage(QDataStream *in){
    QString message;
    QString senderName;
    quint64 senderId;
    *in >> senderName;
    *in >> senderId;
    *in >> message;
    qDebug() << "Text message from server";
    emit messageGot(message, senderId, senderName);
}

void MessengerServer::sendMessageByID(QString message, quint64 id)
{
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out << quint16(0) << quint8(2) << id << message;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
    server->write(arrBlock);
    server->flush();
    qDebug() << "Sent message to the server!" << message;
}

void MessengerServer::slotError(QAbstractSocket::SocketError err){
    QString strError =
            "Error: " + (err == QAbstractSocket::HostNotFoundError ?
                             "The host was not found." :
                             err == QAbstractSocket::RemoteHostClosedError ?
                                 "The remote host is closed." :
                                 err == QAbstractSocket::ConnectionRefusedError ?
                                     "The connection was refused." :
                                     QString(server->errorString())
                                     );
    qWarning() << "Server error: " << strError;
    server->disconnectFromHost();
    timer->start();
    emit serverStatusGot("Unaccessable");
}

void MessengerServer::sendMessage(QString m, quint64 id){
    sendMessageByID(m, id);
}

void MessengerServer::  readUsersInfo(QDataStream *in){
    int usersCount;
    *in >> usersCount;
    qDebug() << "Users info got, count is" << usersCount;
    QMap<QString, ServerClient> list;
    for(int i = 0; i < usersCount; i++){
        ServerClient client;
        QString name, status;
        quint64 id;
        *in >> name >> id >> status;
        client.name = name;
        client.id = id;
        client.status = status;
        list[client.name] = client;
    }
    emit usersDataGot("#users#", list);
}

void MessengerServer::slotSendLoginData(QString name, QString password){
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out << quint16(0) << quint8(1) << name << password;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
    server->write(arrBlock);
    server->flush();
    qDebug() << "Sent login data to the server!";
}

void MessengerServer::sendRegisrationData(QString login, QString password){
    QByteArray arr;
    QDataStream out(&arr, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out << quint16(0) << quint8(10) << login << password;
    out.device()->seek(0);
    out << quint16(arr.size() - sizeof(quint16));
    server->write(arr);
    server->flush();
    qDebug() << "Sent registration data to the server" << login;;
}

void MessengerServer::readRegistrationData(QDataStream *in){
    bool status;
    *in >> status;
    qDebug() << "Read registration data, staus is" << status;
    emit registrationDataGot(status);
}

void MessengerServer::readFriendRequest(QDataStream *in){
    QString name, message;
    quint64 id;
    *in >> id >> name >> message;
    qDebug() << "Chat open request got";
    emit friendRequestGot(name, id, message);
}

void MessengerServer::readFriendRequestAnswer(QDataStream *in){
    QString name, dialogName;
    QString anotherUser = "SERVER";
    quint64 dialogID;
    bool isPublic;
    *in >> isPublic;
    if(!isPublic){
        *in >> name >> dialogID >> dialogName >> anotherUser;
    } else {
        *in >> name >> dialogID >> dialogName;
    }
    qDebug() << "Read chat request answer, public" << isPublic << "dialog name" << dialogName << "Another user is" << anotherUser;
    emit friendRequestAnswerGot(name, dialogID, dialogName, anotherUser);
}

void MessengerServer::sendFriendsRequest(quint64 id, QString message){
    QByteArray arrBlock;
    qDebug() << "Sending chat request " << id << "Message is" << message;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out << quint16(0) << quint8(5) << id << message;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
    server->write(arrBlock);
    server->flush();
}

void MessengerServer::sendFriendsRequestAnswer(quint64 pendingFriend, bool status, QString chatName){
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out << quint16(0) << quint8(6) << status << pendingFriend << chatName;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
    server->write(arrBlock);
    qDebug() << "Sent chatRequest answer" << status;
    server->flush();
}

void MessengerServer::sendSyncRequest(QMap<quint64, QString> hashes){
    QByteArray arr;
    QDataStream out(&arr, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out << quint16(0) << quint8(4);
    out << hashes.size();
    if(hashes.isEmpty()){
        qDebug() << "Hashes emty, sending NULL request";
    } else {
        for(QMap<quint64, QString>::iterator iter = hashes.begin(); iter != hashes.end(); iter++){
            out <<  iter.key() << iter.value();
        }
    }
    out.device()->seek(0);
    out << quint16(arr.size() - sizeof(quint16));
    server->write(arr);
    server->flush();
}

void MessengerServer::readSyncFile(QDataStream *in){
    uint fileSize;
    quint64 fileID;
    *in >> fileID >> fileSize;
    qDebug() << "Got sync file, the name is " << fileID << " , size is " << fileSize;
    char* rawData;
    in->readBytes(rawData, fileSize);
    QByteArray bytes(rawData, fileSize);
    emit syncFileGot(fileID, bytes);
}

void MessengerServer::readPublicChats(QDataStream *in){
    int count;
    *in >> count;
    QMap<QString, ServerClient> list;
    for(int i = 0; i < count; i++){
        ServerClient client;
        QString name;
        quint64 id;
        *in >> id >> name;
        client.name = name;
        client.id = id;
        client.status = "opened";
        list[client.name] = client;
    }
    emit usersDataGot("#publicChats#", list);
    qDebug() << "Read public chats list, numbers of chats" << count;
}

void MessengerServer::slotSendPublicChatJoinRequest(quint64 id){
    QByteArray arr;
    QDataStream out(&arr, QIODevice::WriteOnly);
    out << quint16(0) << quint8(7) << id;
    out.device()->seek(0);
    out << quint16(arr.size() - sizeof(quint16));
    server->write(arr);
    server->flush();
    qDebug() << "Sent publicChat join request";
}
