#include "clientcore.h"

ClientCore::ClientCore(MessengerServer* server)
{    
    loginW = new LoginWindow;
    chatMW = new ChatMW;

    //Signals\slots setup
    connect(server, SIGNAL(authorizationAnswerGot(bool,quint64, QString)),
            SLOT(getAuthData(bool,quint64, QString)));

    connect(loginW, SIGNAL(loginSignal(QString,QString)),
            server, SLOT(slotSendLoginData(QString,QString)));

    connect(loginW->regW, SIGNAL(registerSignal(QString,QString)),
            server, SLOT(sendRegisrationData(QString,QString)));

    connect(server, SIGNAL(registrationDataGot(bool)),
            SLOT(getRegData(bool)));

    connect(loginW, SIGNAL(hostChanged(QString,int)),
            server, SLOT(connectToHost(QString,int)));

    connect(server, SIGNAL(messageGot(QString,quint64,QString)),
            SLOT(getMessage(QString,quint64,QString)));

    connect(server, SIGNAL(usersDataGot(QString, QMap<QString, ServerClient>)),
            SLOT(getNewOnlineData(QString, QMap<QString, ServerClient>)));

    connect(chatMW, SIGNAL(messageWrited(QString,quint64)),
            server, SLOT(sendMessage(QString,quint64)));

    connect(chatMW, SIGNAL(chatSelected(QString)),
            SLOT(slotUserClicked(QString)));

    connect(this, SIGNAL(chatOpenRequest(quint64,QString)),
            server, SLOT(sendFriendsRequest(quint64,QString)));

    connect(this, SIGNAL(sendSyncRequest(QMap<quint64,QString>)),
            server, SLOT(sendSyncRequest(QMap<quint64,QString>)));

    connect(server, SIGNAL(syncFileGot(quint64,QByteArray)),
            this, SLOT(syncFileGot(quint64,QByteArray)));

    connect(this, SIGNAL(sendChatRequestStatus(quint64,bool, QString)),
            server, SLOT(sendFriendsRequestAnswer(quint64,bool, QString)));

    connect(chatMW, SIGNAL(messageWrited(QString,quint64)),
            this, SLOT(slotMessageWrited(QString,quint64)));

    connect(server, SIGNAL(friendRequestAnswerGot(QString,quint64, QString, QString)),
            this, SLOT(getChatRequestStatus(QString,quint64, QString, QString)));

    connect(server, SIGNAL(friendRequestGot(QString,quint64,QString)),
            this, SLOT(getChatRequest(QString,quint64,QString)));

    connect(chatMW, SIGNAL(chatSelectedByID(quint64)),
            this, SLOT(slotOpenedChatSelected(quint64)));

    connect(server, SIGNAL(serverStatusGot(QString)),
            this, SLOT(slotServerError(QString)));

    connect(chatMW->clientsPanel, SIGNAL(chatJoinRequest(QString)),
            this, SLOT(slotPublicChatClicked(QString)));

    connect(this, SIGNAL(sendPublicChatJoinRequest(quint64)),
            server, SLOT(slotSendPublicChatJoinRequest(quint64)));

    connect(chatMW->clientsPanel, SIGNAL(createNewPublic(QString)),
            server, SLOT(slotCreatePublicChat(QString)));

    loginW->show();
    loginW->sendHostData();
}

void ClientCore::getNewOnlineData(QString group, QMap<QString, ServerClient> newOnline){
    if(group ==  "#users#"){
        usersOnline = newOnline;
         permissions = newOnline[youOwnLogin].status;
         if(permissions != "user"){
             chatMW->clientsPanel->newPublic->setEnabled(true);
         } else {
             chatMW->clientsPanel->newPublic->setEnabled(false);
         }
        qDebug() << "Got new users list";
    } else {
        publicChats = newOnline;
        qDebug() << "Got new public chats list";
    }
    chatMW->reDrawList(group, newOnline);
}

void ClientCore::getMessage(QString message, quint64 senderID, QString senderLogin){
    if(!openedChats.contains(senderID))
        return;
    openedChats[senderID]->append(senderLogin, message);
    chatMW->openChat(openedChats[senderID]);
    mfl->writeMessageToChat(senderID, senderLogin, message, youOwnLogin);
}

void ClientCore::slotUserClicked(QString name){
    if (name == youOwnLogin)
        return;
    for(QMap<quint64, Chat*>::iterator it = openedChats.begin(); it != openedChats.end(); it++){
        if(!openedChats.isEmpty() && it.value()->containsAuthor(name) && it.value()->getAuthorsSize() == 2){
            chatMW->openChat(it.value());
            updateChatsList();
            return;
        }
    }
    QString newChatName = QInputDialog::getText(chatMW, "Новый диалог", "Введите название диалога, который вы хотите создать", QLineEdit::Normal);
    if(!newChatName.isEmpty()){
        emit chatOpenRequest(usersOnline[name].id, newChatName);
    }
}

void ClientCore::getAuthData(bool status, quint64 id, QString permissions){
    if(status){
        if(!loginW->isHidden())
            youOwnLogin = loginW->login->text();
        if(permissions != "user"){
            chatMW->clientsPanel->newPublic->setEnabled(true);
        }
        chatMW->show();
        mfl = new MessageFileLogger(youOwnLogin);
        openedChats = mfl->getAllChats();
        updateChatsList();
        emit sendSyncRequest(mfl->getHashes());
    }
    loginW->setAuthorizationStatus(status);
}

void ClientCore::getRegData(bool data){
    if(data){
        youOwnLogin = loginW->regW->login->text();
    }
    loginW->regW->setRegistrationStatus(data);
}

void ClientCore::getServerStatus(QString status){
    loginW->setServerSatatus(status);
}


void ClientCore::getChatRequest(QString name, quint64 id, QString message){
    QString chatInvitation = "Пользователь " + name + " хочет начать с вами чат " + message;
    int n = QMessageBox::question(chatMW, "Новый чат!", chatInvitation, QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    if(n == QMessageBox::Yes){
        emit sendChatRequestStatus(id, true, message);
    } else {
        emit sendChatRequestStatus(id, false, message);
    }
}

void ClientCore::slotMessageWrited(QString message, quint64 dialogID){
    mfl->writeMessageToChat(dialogID, youOwnLogin, message, youOwnLogin);
}

void ClientCore::getChatRequestStatus(QString name, quint64 chatID, QString chatName, QString anotherName){
    if(chatID){
        if(name != "<YOU>"){
            QString message = "Пользователь " + name + " одобрил чат";
            QMessageBox::information(chatMW, "Swallow messenger", message, QMessageBox::Yes);
        }
        Chat* c;
        if(anotherName == "SERVER"){
            c = new Chat(chatID, chatName, true);
        } else {
            QStringList authors;
            authors << youOwnLogin << name;
            c = new Chat(chatID, chatName, authors);
        }
        openedChats[chatID] = c;
        mfl->createChatFile(chatID, chatName, youOwnLogin);
        chatMW->openChat(c);
    }
    updateChatsList();
}

void ClientCore::syncFileGot(quint64 id, QByteArray file){
    mfl->processSync(id, file);
    openedChats = mfl->getAllChats();
    updateChatsList();
    chatMW->openChat(openedChats[id]);
}

void ClientCore::slotOpenedChatSelected(quint64 id){
    chatMW->openChat(openedChats[id]);
}

void ClientCore::updateChatsList(){
    if(!openedChats.isEmpty()){
        for(QMap<quint64, Chat*>::iterator it = openedChats.begin(); it != openedChats.end(); it++){
            if(!it.value()->isPublic)
                chatMW->addChat("#private#", it.value()->name, it.key());
            else
                chatMW->addChat("#public#", it.value()->name, it.key());
        }
    }
}

void ClientCore::slotServerError(QString error){
    if(error == "Unaccessable" && loginW->isHidden()){
        int n = QMessageBox::question(chatMW, "Swallow messenger", "Cоединение потеряно, выйти?", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        if (n == QMessageBox::Yes){
            exit(0);
        } else{
            QMessageBox::information(chatMW, "Swallow messenger", "Ожидайте подключения");
        }
    }
    if(!loginW->isHidden()){
        loginW->setServerSatatus(error);
    }
}

void ClientCore::slotPublicChatClicked(QString name){
    if(openedChats.contains(publicChats[name].id)){
        chatMW->openChat(openedChats[publicChats[name].id]);
        return;
    }
    quint64 id = publicChats[name].id;
    emit sendPublicChatJoinRequest(id);
}
