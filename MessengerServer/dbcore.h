#pragma once

#include <QtSql>
#include <QDebug>

class DBCore
{
    QSqlDatabase db;
private:
    QString getUserData(QString tableName,quint64 id, QString data_type);
    QSqlQuery *query;
    QSqlRecord *record;
    void execute();
public:
    DBCore(QString db_name);
    quint64 authorizeUser(QString login, QString password_hash);
    QString getUserPermissions(quint64 id);
    bool registerUser(QString name, QString password_hash);
    void addUserChat(quint64 user_id, quint64 chat_id);
    bool addPublicChat(quint64 id, QString name);
    void addPublicChatMemeber(quint64 chat, quint64 member);
    void setPermissions(quint64 id, QString permissions);
    QMap<quint64, QString> getPublicChats();
    QList<quint64> getPublicChatMembers(quint64 id);
    QList<quint64> getChats(quint64 id);
};
