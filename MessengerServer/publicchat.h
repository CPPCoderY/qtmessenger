#pragma once

#include <QString>
#include <QList>
#include  "servercore.h"

class ServerCore;

class PublicChat
{
    friend class ServerCore;

    ServerCore* core;
    quint64 id;
    QList<quint64> members;
public:
    PublicChat(ServerCore* core, quint64 id, QString name);
    PublicChat(ServerCore* core, quint64 id, QString name, QList<quint64> members);

    QString name;

    void sendMessage(QString author, QString message);
    void addMember(quint64 member);
};
