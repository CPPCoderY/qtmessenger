#include "dbcore.h"

DBCore::DBCore(QString db_name)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(db_name);
    query = new QSqlQuery(db);
    record = new QSqlRecord;
    if(!db.open()){
        qFatal("Can not open database! Starting server failed!");
    }
    qDebug() << "Database connected!";
    QString s_query("CREATE TABLE IF NOT EXISTS server_users ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    "name varchar(256) NOT NULL,"
                    "password varchar(256) NOT NULL,"
                    "chats varchar(256),"
                    "permissions varchar(256) NOT NULL);");
    query->prepare(s_query);
    execute();
    query->clear();
    s_query.clear();
    s_query = "CREATE TABLE IF NOT EXISTS public_chats ("
              "id INTEGER NOT NULL,"
              "name varchar(256) NOT NULL, "
              "members varchar(256));";
    query->prepare(s_query);
    execute();
    query->clear();
}

bool DBCore::registerUser(QString name, QString password_hash){
    QString s_query("SELECT name FROM server_users;");
    query->prepare(s_query);
    execute();
    QList<QString> registeredNames;
    while(query->next()){
        registeredNames.append(query->value(record->indexOf("name")).toString());
    }
    if(registeredNames.contains(name)){
        return 0;
    }
    query->clear();
    query->prepare("INSERT INTO server_users (name, password, permissions)"
                   "VALUES(:name, :password, :perm);");
    query->bindValue(":name", name);
    query->bindValue(":password", password_hash);
    query->bindValue(":perm", "user");
    execute();
    query->clear();
    return true;
}

QString DBCore::getUserData(QString tableName, quint64 id, QString data_type){
    QString s = "SELECT * FROM " + tableName + " WHERE id=" + QString::number(id);
    query->prepare(s);
    query->bindValue(":table", tableName);
    query->bindValue(":id", id);
    execute();
    query->next();
    QVariant var = query->value(data_type).toString();
    if(!var.isNull()){
        return var.toString();
    }
    query->clear();
    return "";
}

QList<quint64> DBCore::getChats(quint64 id){
    QString s_chats = getUserData("server_users", id, "chats");
    QList<QString> s_l_chats = s_chats.split(" ");
    QList<quint64> result;
    for(QList<QString>::iterator it = s_l_chats.begin(); it != s_l_chats.end(); it++){
        QString s = *it;
        result.append(s.toLongLong());
    }
    return result;
}

quint64 DBCore::authorizeUser(QString login, QString password_hash){
    qDebug() << "Trying to authorize client with login " << login;
    query->prepare("SELECT * FROM server_users WHERE name=:name;");
    query->bindValue(":name", login);
    execute();
    query->first();
    QVariant var = query->value(record->indexOf("password")).toString();
    if(var.isNull()){
        qDebug() << "Database query is null";
        return 0;
    }
    QString bd_hash = var.toString();
    if(bd_hash == password_hash){
        return query->value(record->indexOf("id")).toLongLong();
    }
    return 0;
    query->clear();
}

QString DBCore::getUserPermissions(quint64 id)
{
    return getUserData("server_users", id, "permissions");
}

void DBCore::addUserChat(quint64 user_id, quint64 chat_id){
    query->prepare("SELECT chats FROM server_users WHERE id=:id;");
    query->bindValue(":id", user_id);
    execute();
    query->next();
    QString chats = query->value(record->indexOf("chats")).toString();
    chats.append(" " + QString::number(chat_id));
    QString s_query = "UPDATE server_users SET chats=:c_id WHERE id=:id;";
    query->prepare(s_query);
    query->bindValue(":c_id", chats);
    query->bindValue(":id", user_id);
    execute();
}

bool DBCore::addPublicChat(quint64 id, QString name){
    QString s_query("SELECT name FROM public_chats;");
    query->prepare(s_query);
    execute();
    QList<QString> registeredNames;
    while(query->next()){
        registeredNames.append(query->value(record->indexOf("name")).toString());
    }
    if(registeredNames.contains(name)){
        return false;
    }
    query->prepare("INSERT INTO public_chats (id, name)"
                   "VALUES(:id, :name);");
    query->bindValue(":id", id);
    query->bindValue(":name", name);
    execute();
    query->clear();
    return true;
}

void DBCore::addPublicChatMemeber(quint64 chat, quint64 member){
    query->prepare("SELECT members FROM public_chats WHERE id=:id;");
    query->bindValue(":id", chat);
    execute();
    query->next();
    QString members = query->value(record->indexOf("members")).toString();
    members.append(" " + QString::number(member));
    QString s_query = "UPDATE public_chats SET members=:c_id WHERE id=:id;";
    query->prepare(s_query);
    query->bindValue(":c_id", members);
    query->bindValue(":id", member);
    execute();
}

QList<quint64> DBCore::getPublicChatMembers(quint64 id){
    QString s_chats = getUserData("public_chats", id, "members");
    QList<QString> s_l_chats = s_chats.split(" ");
    QList<quint64> result;
    for(QList<QString>::iterator it = s_l_chats.begin(); it != s_l_chats.end(); it++){
        QString s = *it;
        result.append(s.toLongLong());
    }
    return result;
}

QMap<quint64, QString> DBCore::getPublicChats(){
    QString s_query("SELECT * FROM public_chats;");
    query->prepare(s_query);
    execute();
    QMap<quint64, QString> registeredIDs;
    while(query->next()){
        registeredIDs[query->value(record->indexOf("id")).toLongLong()] = query->value(record->indexOf("name")).toString();
    }
    return registeredIDs;
}

void DBCore::execute(){
    if(!query->exec()){
        qDebug() << "Executing failed! Query is \n" << query->lastQuery() <<
                    "\n Error is " << query->lastError().text();
        qFatal("SQL Error! This error can broke database! STOPPING server!");
    }
    *record = query->record();
}

void DBCore::setPermissions(quint64 id, QString permissions){
    query->prepare("UPDATE server_users SET permissions=:perm WHERE id=:id");
    query->bindValue(":perm", permissions);
    query->bindValue(":id", id);
    execute();
}
