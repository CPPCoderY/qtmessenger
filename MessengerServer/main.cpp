#include <QCoreApplication>
#include <servercore.h>
#include <QDebug>
#include <QThread>
#include <iostream>
#include <string>
#include <QTextStream>
#include <QFile>
#include <QDateTime>

using namespace std;

QString logFileName;

void logMessage(QtMsgType type, const QMessageLogContext &context, const QString& message){
    QFile log(logFileName);
    if(!log.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
        return;
    }
#ifdef DEBUG
    cout << message.toStdString() << endl;
#endif
    QTextStream out(&log);
    switch(type){
    case QtDebugMsg:
        out << "[" << QTime::currentTime().toString() << "]" << "Debug: " << "    " << message << context.file << endl;
        break;
    case QtFatalMsg:
        out << "[" << QTime::currentTime().toString() << "]" << "Fatal Error: " << message << "    " << context.file << endl;
        break;
    case QtWarningMsg:
        out << "[" << QTime::currentTime().toString() << "]" << "Warning" << message << "    " << context.file << endl;
        break;
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    int port = 2323;
    if(argc == 1 && atoi(argv[0]) != 0){
        port = atoi(argv[0]);
    }
    QDateTime time;
    logFileName = QString("[" + time.currentDateTime().toString(Qt::ISODateWithMs) + "] protocol.log");
    qInstallMessageHandler(logMessage);
    ServerCore server(port);
    server.loadPublicChats();
    QThread networkT;
    server.moveToThread(&networkT);
    networkT.start();
    cout << "Welocome to Swallow Messenger Server version 1.6Release(Final)" << endl << "Type 'info' to help" << endl;
#ifdef DEBUG
    cout << "Debug mode enabled" << endl;
#endif
    while(true){
        cout  << "<< ";
        string command;
        cin >> command;
        if(command == "users"){
            server.printAllUsers();
        } else if (command == "newPublic") {
            cout << "Enter chat name: ";
            string chatName;
            cin >> chatName;
            QString name = QString::fromStdString(chatName);
            quint64 status = server.createPublicChat(name);
            if(status){
                cout << "New public chat created" << endl;
            } else {
                cout << "Chat with same name already exists! " << endl;
            }
        } else if (command == "info"){
            cout << "Swallow Messenger Server version 1.6 Final" <<  endl
                 << "Developed by Andrey Bogdanov 2017 St.Petersburg" << endl
                 << "Availabale commands: " << endl
                 << "'users' - prints all connected users and there IDs" << endl\
                 << "'newPublic public_name' - change 'public_name to' to any custom name," << endl <<" creates new public chat" << endl
                 << "'op' - set oprator permissions to user" << endl
                 << "'deop' - remove operator permissions of user" << endl;
        } else if(command == "quit"){
            server.closeServer();
            exit(0);
        } else if(command == "op"){
            int id;
            cout << "Enter id: ";
            cin >>  id;
            if(server.clientsList.contains(id)){
                server.setPermissions(id,  "operator");
                cout << "Permissions of " << server.clientsList[id]->name.toStdString() << " changed to 'operator'" << endl;
            } else {
                cout << "User with id " << id << " not found!" << endl;
            }
        }else if(command == "deop"){
            int id;
            cout << "Enter id: ";
            cin >>  id;
            if(server.clientsList.contains(id)){
                server.setPermissions(id,  "user");
                cout << "Permissions of " << server.clientsList[id]->name.toStdString() << " changed to 'user'" << endl;
            } else {
                cout << "User with id " << id << " not found!" << endl;
            }
        } else {
            cout << "Incorrect command" << endl;
            continue;
        }
    }
    return app.exec();
}
