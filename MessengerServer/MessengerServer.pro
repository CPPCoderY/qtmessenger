QT += core network sql xml
CONFIG += c++11

TARGET = SwallowMessengerServer
CONFIG += console
CONFIG += release
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    servercore.cpp \
    serverclient.cpp \
    dbcore.cpp \
    messagefilelogger.cpp \
    publicchat.cpp

HEADERS += \
    servercore.h \
    serverclient.h \
    dbcore.h \
    messagefilelogger.h \
    publicchat.h
