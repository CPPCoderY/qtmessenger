#include "messagefilelogger.h"

MessageFileLogger::MessageFileLogger() {
    QString dirName = QDir::currentPath() + "/clientsChats/";
    dirName = QDir::toNativeSeparators(dirName);
    QDir dir(dirName);
    if(!dir.exists()){
        qDebug() << "Chats direcotory does not exists.";
        dir.mkdir(dir.path());
    }
}

void MessageFileLogger::createChatFile(quint64 id, QString chatName, bool isPublic){
    QString fileName = QDir::current().absolutePath() + "/clientsChats/" + QString::number(id) + ".chh";
    fileName = QDir::toNativeSeparators(fileName);
    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    QTextStream writer(&file);
    QXmlStreamWriter xml(&file);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeStartElement("dialogs");
    xml.writeAttribute("name", chatName);
    xml.writeAttribute("isPublic", QString::number(qint8(isPublic)));
    writer << ">";
    file.close();
}

QString MessageFileLogger::getChatHash(quint64 chatID){
    QString fileName = QDir::current().absolutePath() + "/clientsChats/" + QString::number(chatID) + ".chh";
    fileName = QDir::toNativeSeparators(fileName);
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    QByteArray arr = file.readAll();
    QString hash = QString(QCryptographicHash::hash(arr, QCryptographicHash::Sha256).toHex());
    return hash;
}

QByteArray MessageFileLogger::getChat(quint64 id){
    QString fileName = QDir::current().absolutePath() + "/clientsChats/" + QString::number(id) + ".chh";
    fileName = QDir::toNativeSeparators(fileName);
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    QByteArray arr = file.readAll();
    return arr;
}

void MessageFileLogger::writeMessage(quint64 chatID, QString author, QString message){
    QString fileName = QDir::current().absolutePath() + "/clientsChats/" + QString::number(chatID) + ".chh";
    fileName = QDir::toNativeSeparators(fileName);
    QFile file(fileName);
    file.open(QIODevice::Append);
    QXmlStreamWriter xml(&file);
    xml.setAutoFormatting(true);
    xml.writeStartElement("message");
    xml.writeAttribute("author", author);
    xml.writeCharacters(message);
    xml.writeEndElement();
}
