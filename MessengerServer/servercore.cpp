#include "servercore.h"

ServerCore::ServerCore(int port)
{
    dataBase = new DBCore("MessengerServerDB");
    loadPublicChats();
    server = new QTcpServer(this);
    if(!server->listen(QHostAddress::Any, port)){
        server->close();
        qDebug() << server->errorString();
        qFatal("Unable to start server.");
    }
    connect(server, SIGNAL(newConnection()), this, SLOT(slotConnectUser()));
    qDebug() << "Server started on port " << port;
}

void ServerCore::slotConnectUser(){
    QTcpSocket* clientSocket = server->nextPendingConnection();
    quint64 id = connectedClients.size() + 1;
    ServerClient*  client = new ServerClient(this, clientSocket, id);
    connect(clientSocket, SIGNAL(disconnected()),
            clientSocket, SLOT(deleteLater()));
    connect(clientSocket, SIGNAL(disconnected()), client, SLOT(destroyUser()));
    connect(clientSocket, SIGNAL(readyRead()),
            client, SLOT(handleMessage()));
    connectedClients[id] = client;
    qDebug() << "Connected new user!";
}

bool ServerCore::authorizeClient(QString login, QString password, quint64& tempID){
    quint64 a_id = dataBase->authorizeUser(login, password);
    if(clientsList.contains(a_id)){
        return false;
    }
    if(a_id){
        ServerClient* tempC = connectedClients[tempID];
        connectedClients.remove(tempID);
        clientsList[a_id] = tempC;
        tempC->id = a_id;
        tempC->chats = dataBase->getChats(a_id);
        tempC->permissions = dataBase->getUserPermissions(a_id);
        return true;
    } else {
        return false;
    }
}

bool ServerCore::sendMessageToUser(QString &message, quint64 address, QString senderLogin, quint64 senderID){
    for(QMap<quint64, ServerClient*>::iterator it = clientsList.begin(); it !=  clientsList.end(); it++){
        if(it.value()->chats.contains(address) && it.value()->id != senderID){
            it.value()->sendMessage(message, senderLogin, address);
        }
    }
}

void ServerCore::handleSyncRequest(quint64 id, QMap<quint64, QString> client_files){
    QList<quint64> chats_list = clientsList[id]->chats;
    if(chats_list.isEmpty()){
        return;
    }
    QList<quint64>::iterator it = chats_list.begin();
    for(; it != chats_list.end(); it++){
        quint64 t_chatID = *it;
        if(client_files.isEmpty() || !client_files.contains(t_chatID) ||
                mfl.getChatHash(t_chatID) != client_files[t_chatID] && t_chatID != 0){
            clientsList[id]->sendSyncFile(t_chatID, mfl.getChat(t_chatID));
        }
    }
}

bool ServerCore::registerClient(QString login, QString password){
    return  dataBase->registerUser(login, password);
}

void ServerCore::updateClientSideOnline(){
    for(QMap<quint64, ServerClient*>::iterator it = clientsList.begin(); it !=  clientsList.end(); it++){
        it.value()->sendClientsOnline(clientsList);
    }
}

void ServerCore::setPermissions(quint64 id, QString permissions)
{
    dataBase->setPermissions(id, permissions);
    clientsList[id]->permissions = permissions;
    updateClientSideOnline();
}

void ServerCore::sendPublicChats(){
    for(QMap<quint64, ServerClient*>::iterator it = clientsList.begin(); it != clientsList.end(); it++){
        it.value()->sendPublicChats(publicChats);
    }
}

quint64 ServerCore::createPublicChat(QString name){
    for(QMap<quint64, PublicChat*>::iterator it = publicChats.begin(); it !=  publicChats.end(); it++){
        if(it.value()->name == name)
            return false;
    }
    quint64 id = generateChatId();
    PublicChat* newChat = new PublicChat(this, id, name);
    publicChats[id] = newChat;
    dataBase->addPublicChat(id, name);
    sendPublicChats();
    mfl.createChatFile(id, name, true);
    return id;
}

quint64 ServerCore::addChat(quint64 id, quint64 pending_friend, QString chatName){
    quint64 newChatID = generateChatId();
    dataBase->addUserChat(id, newChatID);
    dataBase->addUserChat(pending_friend, newChatID);
    clientsList[id]->chats.append(newChatID);
    clientsList[pending_friend]->chats.append(newChatID);
    qDebug() << "Adding new chat, with id " << newChatID;
    return newChatID;
}

quint64 ServerCore::generateChatId(){
    QSettings settings;
    quint64 id = settings.value("/Settings/IDsCounter", 0).toLongLong();
    if(!id)
        id = 1;
    else
        id++;
    settings.setValue("/Settings/IDsCounter", id);
    return id;
}

void ServerCore::printAllUsers(){
    for(QMap<quint64, ServerClient*>::iterator it = clientsList.begin(); it !=  clientsList.end(); it++){
        std::cout << "User " << it.value()->name.toStdString() << ", ID" << it.key() <<  ",  Permissions " << it.value()->permissions.toStdString() << std::endl;
    }
    if(clientsList.isEmpty()){
        std::cout << "There is no users on this server" << std::endl;
    }
}

void ServerCore::loadPublicChats(){
    QMap<quint64, QString> ids = dataBase->getPublicChats();
    if(!ids.isEmpty()){
        for(QMap<quint64, QString>::iterator it = ids.begin(); it != ids.end(); it++){
            QList<quint64> members = dataBase->getPublicChatMembers(it.key());
            PublicChat* c = new PublicChat(this, it.key(), it.value(), members);
            publicChats[it.key()] = c;
        }
    }
    sendPublicChats();
}

void ServerCore::addUserToPublicChat(quint64 id, quint64 chatID){
    if(!publicChats[chatID]->members.contains(id)){
        publicChats[chatID]->addMember(id);
        clientsList[id]->chats.append(chatID);
    }
}

void ServerCore::closeServer(){
    server->close();
}
