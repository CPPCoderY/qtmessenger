#include "publicchat.h"

PublicChat::PublicChat(ServerCore *core, quint64 id, QString name) : core(core), id(id), name(name)
{
}

PublicChat::PublicChat(ServerCore *core, quint64 id, QString name, QList<quint64> members) : id(id), core(core), name(name), members(members)
{
}

void PublicChat::addMember(quint64 member){
    members.append(member);
    core->dataBase->addPublicChatMemeber(id, member);
}

void PublicChat::sendMessage(QString author, QString message){
    core->mfl.writeMessage(id, author, message);
    for(QList<quint64>::iterator it = members.begin(); it != members.end(); it++){
        if(core->clientsList.contains(*it)){
            core->clientsList[*it]->sendMessage(message, author, id);
        }
    }
}
