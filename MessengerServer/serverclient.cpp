#include "serverclient.h"

ServerClient::ServerClient(ServerCore *server, QTcpSocket *sentSocket, quint64 tempId)
    : core(server),
      socket(sentSocket),
      id(tempId)
{
}

void ServerClient::handleMessage(){
    QDataStream *in = new QDataStream(socket);
    in->setVersion(QDataStream::Qt_5_3);
    quint8 message_type;
    *in >> nextBlockSize;
    for(;;){
        if(!nextBlockSize){
            if(socket->bytesAvailable() < sizeof(quint16)){
                break;
            }
            *in >> nextBlockSize;
        }
        if(socket->bytesAvailable() < nextBlockSize) {
            break;
        } else {
            QString m;
            quint64 address;
            *in >> message_type;
            qDebug() << "Got message type " << message_type;
            switch(message_type){
            case 1:
                login(in);
                break;
            case 2:
                readMessage(in, m, address);
                core->mfl.writeMessage(address, name ,m);
                core->sendMessageToUser(m, address, name, id);
                break;
            case 4:
                processSyncRequest(in);
                break;
            case 5:
                processChatRequest(in);
                break;
            case 6:
                readChatRequestStatus(in);
                break;
            case 7:
                processJoinRequest(in);
                break;
            case 10:
                processRegistrationData(in);
                break;
            case 11:
                processChatCreatinngRequest(in);
                break;
            default:
                qWarning("Incorrect message type!");
                break;
            }
            nextBlockSize = 0;
        }
    }
    delete in;
}

void ServerClient::login(QDataStream *in){
    QString login;
    QString password;
    *in >> login;
    *in >> password;
    if(core->authorizeClient(login, password, id)){
        sendLoginStatus(true);
        name = login;
        qDebug() << "Logined, login is" << name;
        sendPublicChats(core->publicChats);
        core->updateClientSideOnline();
    } else {
        sendLoginStatus(false);
        qDebug() << "Authorization of " << login << "failed!";
    }
}

void ServerClient::sendLoginStatus(bool status){
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out << quint16(0) << quint8(1) << status;
    if(status)
        out << permissions;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
    socket->write(arrBlock);
    socket->flush();
    qDebug() << "Sent" << status << "login status";
}

void ServerClient::readMessage(QDataStream* in, QString& message, quint64& address){
    *in >> address;
    *in >> message;
    qDebug() << "Readed message! Dialog id is " << address << " message " << message;
}

void ServerClient::sendMessage(QString &message, QString senderLogin, quint64 senderID){
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out << quint16(0) << quint8(2) << senderLogin << senderID << message;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
    socket->write(arrBlock);
    socket->flush();
    qDebug() << "Sent message to" << name;
}

void ServerClient::sendClientsOnline(QMap<quint64, ServerClient*> clientsList){
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out <<  quint16(0) << quint8(3) << clientsList.size();
    for(QMap<quint64, ServerClient*>::iterator it = clientsList.begin(); it !=  clientsList.end(); it++){
        out << it.value()->name << it.key() << it.value()->permissions;
    }
    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
    socket->write(arrBlock);
    socket->flush();
    qDebug() << "Sent online users to " << name;
}

void ServerClient::destroyUser(){
    core->clientsList.remove(id);
    if(core->connectedClients.contains(id))
        core->connectedClients.remove(id);
    core->updateClientSideOnline();
    qDebug() << "User" << name <<" disconnected";
}

void ServerClient::processRegistrationData(QDataStream *in){
    QString login, password;
    *in >> login >> password;
    qDebug() << "Obtained registration packet, name is" << login;
    bool r_status = core->registerClient(login, password);
    if(r_status){
        sendRegistrationData(true);
        core->authorizeClient(login, password, id);
        name = login;
        sendLoginStatus(true);
        core->updateClientSideOnline();
        sendPublicChats(core->publicChats);
    } else {
        sendRegistrationData(false);
    }
}

void ServerClient::processChatCreatinngRequest(QDataStream *in)
{
    QString chatName;
    *in >> chatName;
    core->addUserToPublicChat(id, core->createPublicChat(chatName));
}

void ServerClient::sendRegistrationData(bool status){
    QByteArray arr;
    QDataStream out(&arr, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_3);
    out << quint16(0) << quint8(10) << status;
    out.device()->seek(0);
    out << quint16(arr.size() - sizeof(quint16));
    socket->write(arr);
    socket->flush();
    qDebug() << "Sent registration status to id " << id << " status is " << status;
}

void ServerClient::processSyncRequest(QDataStream *in){
    QMap<quint64, QString> hashes;
    int h_count;
    *in >> h_count;
    for(int i = 0; i < h_count;  i++){
        quint64 c_id;
        QString hash;
        *in >> c_id >> hash;
        hashes[c_id] = hash;
    }
    core->handleSyncRequest(id, hashes);
}

void ServerClient::sendSyncFile(quint64 chatID, QByteArray file){
    if(chatID && file.size() != 0){
        QByteArray arr;
        QDataStream out(&arr, QIODevice::WriteOnly);
        out << quint16(0) << quint8(4) << chatID << uint(file.size()) << file;
        out.device()->seek(0);
        out << quint16(arr.size() - sizeof(quint16));
        socket->write(arr);
        socket->flush();
        qDebug() << "Sent file to client, size " << file.size() << " Client id is " << id;
    } else {
        qDebug() << "File emty ignoring it...";
    }
}

void ServerClient:: processChatRequest(QDataStream *in){
    qDebug() << "Chat open request from" << name;;
    quint64 pendingChat;
    QString requestMessage;
    *in >> pendingChat >> requestMessage;
    core->clientsList[pendingChat]->sendChatRequest(id, name, requestMessage);
}

void ServerClient::sendChatRequest(quint64 sender, QString senderName, QString invite_message){
    QByteArray arr;
    QDataStream out(&arr, QIODevice::WriteOnly);
    out << quint16(0) << quint8(5) << sender << senderName << invite_message;
    out.device()->seek(0);
    out << quint16(arr.size() - sizeof(quint16));
    socket->write(arr);
    socket->flush();
    qDebug() << "Wrote chat request to " << name << " from " << senderName << "chat name" << invite_message;
}

void ServerClient::readChatRequestStatus(QDataStream *in){
    bool status;
    quint64 initiator;
    QString chatName;
    *in >> status >> initiator >>  chatName;
    if(status){
        quint64 dialog = core->addChat(id, initiator, chatName);
        core->clientsList[initiator]->sendChatRequestStatus(name, dialog, chatName, name);
        sendChatRequestStatus("<YOU>", dialog, chatName, name);
        core->mfl.createChatFile(dialog, chatName, false);
        return;
    }
    if(core->clientsList.contains(initiator))
        core->clientsList[initiator]->sendChatRequestStatus(name, 0, chatName, name);
}

void ServerClient::sendChatRequestStatus(QString name, quint64 dialogID, QString chatName, QString answerName){
    QByteArray arr;
    QDataStream out(&arr, QIODevice::WriteOnly);
    if(answerName == " "){
        out << quint16(0) << quint8(6) <<  true << name << dialogID << chatName;
        qDebug() << "Sent public chat request status to user" << name;
    }else{
        out << quint16(0) << quint8(6) <<  false << name << dialogID << chatName << answerName;
        qDebug() << "Sent private chat request status to user" << name;
    }
    out.device()->seek(0);
    out << quint16(arr.size() - sizeof(quint16));
    socket->write(arr);
    socket->flush();
}

void ServerClient::processJoinRequest(QDataStream *in){
    quint64 chatToJoin;
    *in >> chatToJoin;
    core->addUserToPublicChat(id, chatToJoin);
    sendChatRequestStatus("<YOU>", chatToJoin, core->publicChats[chatToJoin]->name);
    sendSyncFile(chatToJoin, core->mfl.getChat(chatToJoin));
    core->dataBase->addUserChat(id, chatToJoin);
    QString joinMessage = "User " + name + " joined the chat";
    core->sendMessageToUser(joinMessage, chatToJoin, name, id);
    qDebug() << "User" << name << "joined to" << chatToJoin;
}

void ServerClient::sendPublicChats(QMap<quint64, PublicChat *> publicChats){
    QByteArray arr;
    QDataStream out(&arr, QIODevice::WriteOnly);
    out << quint16(0) << quint8(8) << publicChats.size();
    for(QMap<quint64, PublicChat*> ::iterator it = publicChats.begin();  it != publicChats.end(); it++){
        out << it.key() << it.value()->name;
    }
    out.device()->seek(0);
    out << quint16(arr.size() - sizeof(quint16));
    socket->write(arr);
    socket->flush();
}
