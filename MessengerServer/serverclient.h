#pragma once

#include <QString>
#include <QObject>
#include <QTcpSocket>
#include <QByteArray>
#include <QDataStream>

#include "servercore.h"
#include "publicchat.h"

class ServerCore;
class PublicChat;

class ServerClient : public QObject
{
    Q_OBJECT
private:
    ServerCore* core;
    quint16 nextBlockSize;

    //Read from the input stream
    void readMessage(QDataStream *in, QString& message, quint64& id);
    void processSyncRequest(QDataStream *in);
    void processRegistrationData(QDataStream *in);
    void processChatCreatinngRequest(QDataStream* in);
    void processChatRequest(QDataStream *in);
    void readChatRequestStatus(QDataStream *in);
    void processJoinRequest(QDataStream *in);

    //Send to the output stream
    void sendLoginStatus(bool status);
    void sendChatRequestStatus(QString name, quint64 dialogID, QString chatName, QString answerName = " ");
    void sendChatRequest(quint64 sender, QString senderName, QString invite_message);
    void sendRegistrationData(bool status);

    //Reads login data, calls core's authorize() method, writes result
    void login(QDataStream *in);

public:
    QTcpSocket* socket;

    QList<quint64> chats;
    QString name;
    QString permissions;
    quint64 id;

    ServerClient(ServerCore* server, QTcpSocket* sentSocket, quint64 tempId);

     void sendMessage(QString& message, QString senderLogin, quint64 senderID);
     void sendClientsOnline(QMap <quint64, ServerClient*> clientsList);
     void sendPublicChats(QMap<quint64, PublicChat*> publicChats);
     void sendSyncFile(quint64 chatID, QByteArray file);

public slots:
    void handleMessage();
    void destroyUser();
};
