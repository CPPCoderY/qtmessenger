#pragma once

#include <QString>
#include <QFile>
#include <QtXml>
#include <QCryptographicHash>

class MessageFileLogger
{

public:
    MessageFileLogger();
    void writeMessage(quint64 chatID, QString author, QString message);
    void createChatFile(quint64 id, QString chatName, bool isPublic = false);
    QString getChatHash(quint64 chatID);
    QByteArray getChat(quint64 id);
};
