#pragma once

#include <QTcpServer>
#include <QMap>
#include <QSettings>
#include <iostream>

#include "serverclient.h"
#include "messagefilelogger.h"
#include "dbcore.h"
#include "publicchat.h"

class ServerClient;
class QTcpSocket;

class ServerCore : public QObject
{
    friend class ServerClient;
    friend class PublicChat;
    Q_OBJECT
private:
    DBCore* dataBase;

    MessageFileLogger mfl;

    //Unauthorized clients
    QMap <quint64, ServerClient*> connectedClients;

    //Authorized clients

    QMap<quint64, PublicChat*> publicChats;

    QTcpServer* server;

    quint64 generateChatId();

public:
    ServerCore(int port);

    QMap <quint64, ServerClient*> clientsList;

    //Manipulates with users objects
    bool authorizeClient(QString login, QString password, quint64& tempID);
    bool sendMessageToUser(QString& message, quint64 address, QString senderLogin, quint64 senderID);
    void updateClientSideOnline();
    void setPermissions(quint64 id, QString permissions);

    void handleSyncRequest(quint64 id, QMap<quint64, QString> client_files);

    void sendPublicChats();
    quint64 createPublicChat(QString name);
    void loadPublicChats();
    void addUserToPublicChat(quint64 id, quint64 chatID);

    void printAllUsers();

    //User registaration
    bool registerClient(QString login, QString password);
    quint64 addChat(quint64 id, quint64 pending_friend , QString chatName);

    void closeServer();

private slots:
    void slotConnectUser();
};
